// StringProject.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>

int main()
{
    /*std::string name("Wasasdafdqefedaq");
    std::cout << name << "\n";

    std::cout << "Length: " << name.length() << "\n";
    std::cout << "1st symbol: " << name[0] << "\n";
    std::cout << "Last symbol: " << name[name.length() - 1] << "\n";*/

    

    std::cout << "Enter your name: ";
    std::string name;
    //std::cin >> name;
    std::getline(std::cin, name);

    std::cout << "Enter your age: ";
    std::string age;
    //std::cin >> age;
    std::getline(std::cin, age);

    std::cout << name << " " << age << "\n" << "\n";

    std::cout << "Name string length: " << name.length() << " symbols"<< std::endl;
    std::cout << "Name 1st symbol: " << name[0] << std::endl;
    std::cout << "Name last symbol: " << name[name.length() - 1] << "\n" << "\n";
    
    std::cout << "Age string length: " << age.length() << " symbols" << std::endl;
    std::cout << "Age 1st symbol: " << age[0] << std::endl;
    std::cout << "Age last symbol: " << age[age.length() - 1] << std::endl;
    
    
    return 0;

}
